KBP-ProPPR
==========

Tools for building a ProPPR dataset from the KBP Entity Linking task data.

dname: KBP document name

did: ProPPR document numeric ID

qid: Query ID like EL_000 or EDL14_000

eid: Entity ID like E000

np: Noun Phrase. NPs may contain spaces but not tabs.

## Generate DocIDs, NER tags (=queries), and Tokens from Source Documents ##

**Executable:** edu.cmu.ml.proppr.generate.sourcedocuments.GenerateNERTags

**Input:** classifiers/english.all.3class.distsim.crf.ser.gz sourceDocumentXmlFile1.gz sourceFile2.gz ...

**Output:** stdout with lines like



```
#!txt

DOC	1	bolt-eng-DF-170-181103-15978491
NER	1	7	9	US	LOCATION
NER	1	25	27	US	LOCATION
NER	1	243	255	Barack Obama	PERSON
NER	1	261	297	Office Of White House Communications	ORGANIZATION
NER	1	328	333	MSNBC	ORGANIZATION
TOK	1	thats
TOK	1	president
TOK	1	holding
TOK	1	party
TOK	1	discharged
```

Fields: **DOC**|**NER**|**TOK** -tab- **did** -tab- ...

* DOC fields: **dname**
* NER fields: **start** -tab- **end** -tab- **np** -tab- **NER tag**
* TOK fields: **token**

## Generate TSV query file from XML ##

**Executable:** edu.cmu.ml.proppr.generate.queries.GenerateQueries

**Input:** XML query file

**Output:** stdout with lines like


```
#!txt

EDL14_ENG_TRAINING_0001	bolt-eng-DF-170-181122-8792777	22103	22110	Xenophon
EDL14_ENG_TRAINING_0002	APW_ENG_20090826.0903	340	347	Richmond
EDL14_ENG_TRAINING_0003	eng-NG-31-141808-8670849	2021	2041	Newark Teachers Union
EDL14_ENG_TRAINING_0004	eng-NG-31-128482-9270741	3527	3535	Democrats
EDL14_ENG_TRAINING_0005	bolt-eng-DF-199-192783-6834959	37314	37322	talk72000
EDL14_ENG_TRAINING_0006	bolt-eng-DF-200-192410-4742742	1323	1327	Reiss
EDL14_ENG_TRAINING_0007	eng-NG-31-141847-9972870	4786	4803	DenverChannel.com.
EDL14_ENG_TRAINING_0008	bolt-eng-DF-199-192783-6872743	1300	1302	USA
EDL14_ENG_TRAINING_0009	bolt-eng-DF-199-192783-6834959	33561	33568	Japanese
EDL14_ENG_TRAINING_0010	XIN_ENG_20101006.0290	1520	1527	Richmond
```

Fields: **qid** -tab- **dname** -tab- **start** -tab- **end** -tab- **np**
