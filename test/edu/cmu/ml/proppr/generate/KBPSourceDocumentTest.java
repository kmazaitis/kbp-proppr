package edu.cmu.ml.proppr.generate;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.ParsingException;
import nu.xom.Serializer;
import nu.xom.Text;
import nu.xom.ValidityException;

import org.junit.Test;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import edu.cmu.ml.proppr.generate.sourcedata.GenerateNERTags;
import edu.cmu.ml.proppr.generate.sourcedata.TagSoupGenerator;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOC;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.HEADLINE;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.ObjectFactory;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POST;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POSTDATE;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POSTER;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TEXT;


public class KBPSourceDocumentTest {
	
	class Solution {
		int start;
		int end;
		String str;
		Solution(int start, int end, String str) {
			this.start=start;
			this.end=end;
			this.str=str;
		}
		public String toString(){ return new StringBuilder().append(start).append("\t").append(end).append("\t").append(str).toString(); }
	}
	
	Solution[] eng = {
			new Solution(158,166,"Bin Laden"),
			new Solution(174,179,"Manson"),
			new Solution(230,241,"Robert Cohen"),
			new Solution(349,353,"osama"),
			new Solution(512,516,"msnbc"),
			new Solution(541,554,"charles manson"),
			new Solution(597,602,"google"),
			new Solution(606,613,"you tube"),
			new Solution(629,634,"manson"),
			new Solution(640,648,"bin laden"),
			new Solution(1163,1169,"chazwin"),
			new Solution(1252,1257,"Manson"),
			new Solution(1302,1310,"Bin Laden"),
			new Solution(1415,1426,"Robert Cohen"),
			new Solution(1765,1778,"marilyn manson"),
			new Solution(1823,1836,"charles manson"),
			new Solution(1892,1907,"vincent bugliosi"),
			new Solution(1969,1982,"charles manson"),
			new Solution(2034,2047,"Charles_Manson"),
	};
	
	Solution[] xin = {
			new Solution(58,65,"Joe Cole"),
			new Solution(71,85,"Michael Ballack"),
			new Solution(96,102,"Chelsea"),
			new Solution(127,132,"LONDON"),
			new Solution(174,181,"Joe Cole"),
			new Solution(187,201,"Michael Ballack"),
			new Solution(219,225,"English"),
			new Solution(219,233,"English Premier"),
			new Solution(245,251,"Chelsea"),
			new Solution(360,363,"Cole"),
			new Solution(378,384,"Chelsea"),
			new Solution(443,457,"West Ham United"),
			new Solution(480,495,"Roman Abramovich"),
			new Solution(545,551,"Chelsea"),
			new Solution(587,593,"England"),
			new Solution(612,623,"South Africa"),
			new Solution(663,668,"German"),
			new Solution(678,684,"Ballack"),
			new Solution(697,703,"Chelsea"),
			new Solution(761,767,"Ballack"),
			new Solution(793,799,"Chelsea"),
			new Solution(853,859,"Ballack"),
			new Solution(880,886,"Germany"),
			new Solution(978,979,"FA"),
			new Solution(1008,1014,"Ballack"),
			new Solution(1081,1087,"Chelsea")
	};
	
	Solution[] bolt= {
			new Solution(98,107,"random3434"),
		new Solution(563,567,"Yahoo"),
			new Solution(601,610,"WillowTree"),
		new Solution(673,680,"Brittany"),
		new Solution(690,696,"Madonna"),
			new Solution(828,834,"xsited1"),
		new Solution(961,970,"Dave Barry"),
		new Solution(997,1006,"Dave Barry"),
		new Solution(1010,1020,"MiamiHerald"),
			new Solution(1052,1060,"uscitizen"),
		new Solution(1117,1126,"Dave Barry"),
			new Solution(1164,1173,"random3434"),
		new Solution(1417,1426,"Dave Barry"),
			new Solution(1459,1464,"xotoxi"),
			new Solution(1737,1744,"blastoff"),
			new Solution(2133,2140,"Navy1960"),
		new Solution(2483,2486,"Iraq"),
			new Solution(2510,2517,"Navy1960"),
		new Solution(2726,2739,"George W. Bush"),
		new Solution(2843,2854,"Barack Obama"),
			new Solution(2878,2885,"Navy1960"),
			new Solution(3235,3242,"Navy1960"),
			new Solution(3349,3357,"Old Rocks"),
		new Solution(3701,3706,"Hitler"),
		new Solution(3874,3882,"Werhmacht"),
			new Solution(4161,4168,"Xenophon"),
			new Solution(4259,4265,"xsited1"),
			new Solution(4448,4455,"Xenophon"),
			new Solution(4529,4533,"elvis"),
			new Solution(4634,4642,"JW Frogen"),
			new Solution(5054,5059,"Luissa"),
			new Solution(5340,5347,"Navy1960"),
			new Solution(5979,5986,"Navy1960"),
			new Solution(6409,6417,"Oscar Wao"),
			new Solution(6678,6685,"Navy1960"),
		new Solution(6729,6733,"Oscar"),
			new Solution(6948,6956,"Oscar Wao"),
		new Solution(7452,7455,"Paul"),
		new Solution(7459,7463,"Oscar"),
			new Solution(7547,7554,"Navy1960"),
		new Solution(7706,7709,"Paul"),
			new Solution(7890,7895,"Luissa"),
			new Solution(8361,8369,"Old Rocks"),
		new Solution(9440,9445,"Isreal"),
		new Solution(9530,9533,"Cuba"),
			new Solution(9710,9717,"Navy1960"),
		new Solution(11167,11175,"San Diego"),
			new Solution(11684,11691,"Navy1960"),
			new Solution(11914,11919,"Luissa"),
			new Solution(13720,13728,"Old Rocks"),
		new Solution(16535,16540,"Oregon"),
			new Solution(16708,16712,"Annie"),
		new Solution(16995,17007,"Boston Herald"),
		new Solution(16995,17000,"Boston"),
		new Solution(17741,17745,"AC/DC"),
		new Solution(17754,17759,"Moscow"),
		new Solution(19553,19556,"Aden"),
		new Solution(19790,19795,"Boston"),
		new Solution(19953,19958,"Boston"),
		new Solution(19953,19965,"Boston Herald"),
			new Solution(22103,22110,"Xenophon"),
			new Solution(22463,22471,"Old Rocks"),
			new Solution(22904,22912,"Oscar Wao")
};
	Solution[] bolt2 = {
			new Solution(59,64,"London"),
			new Solution(78,83,"London"),
			new Solution(160,164,"Timon"),
			new Solution(287,292,"London"),
			new Solution(287,293,"London,"),
			new Solution(320,325,"London"),
			new Solution(520,525,"Oxford"),
			new Solution(530,538,"Cambridge"),
			new Solution(1033,1038,"London"),
			new Solution(1247,1252,"London"),
			new Solution(1370,1375,"London"),
			new Solution(1427,1430,"Clip"),
			new Solution(1557,1565,"Liverpool"),
			new Solution(1597,1606,"Islington."),
			new Solution(1667,1672,"Putney"),
			new Solution(1749,1760,"Yummy Mummy."),
			new Solution(1784,1796,"yellowmeringu"),
			new Solution(1923,1928,"Barden"),
			new Solution(1971,1978,"Richmond"),
			new Solution(2002,2008,"ish90an"),
			new Solution(2104,2109,"AP1989"),
			new Solution(2268,2283,"South Kensington"),
			new Solution(2288,2297,"Islington."),
			new Solution(2367,2373,"Steph90"),
			new Solution(3401,3412,"Lord_Farquad"),
			new Solution(3523,3529,"peckham"),
			new Solution(3573,3579,"Molokai"),
			new Solution(3667,3670,"Clip"),
			new Solution(3772,3774,"Zoe"),
			new Solution(3779,3786,"Geneviev"),
			new Solution(3849,3850,"Ed"),
			new Solution(3880,3885,"Hoxton"),
			new Solution(3880,3893,"Hoxton and Dan"),
			new Solution(3891,3893,"Dan"),
			new Solution(4138,4149,"Muswell Hill"),
			new Solution(4268,4279,"Muswell Hill"),
			new Solution(4362,4371,"Crouch End"),
			new Solution(4383,4394,"Muswell Hill"),
			new Solution(4441,4450,"Kate Kuba."),
			new Solution(4480,4487,"Highgate"),
			new Solution(4547,4551,"Timon"),
			new Solution(4613,4620,"Phillipa"),
			new Solution(4625,4632,"Penelope"),
			new Solution(4856,4866,"Royal Ascot"),
			new Solution(4905,4920,"Tha_Black_Shinob"),
			new Solution(4965,4976,"Notting Hill"),
			new Solution(4979,4986,"Richmond"),
			new Solution(4989,4998,"Islington,"),
			new Solution(5000,5008,"Stockwell"),
			new Solution(5031,5036,"lekky"),
			new Solution(6069,6082,"Parson's Green"),
			new Solution(6087,6093,"Fullham"),
			new Solution(6123,6130,"Surbiton"),
			new Solution(6135,6144,"Teddington"),
			new Solution(6810,6817,"Highgate"),
			new Solution(6822,6829,"Richmond"),
			new Solution(7017,7025,"Genevieve"),
			new Solution(7030,7033,"Lucy"),
			new Solution(7049,7053,"Cait."),
			new Solution(7073,7077,"Penny"),
			new Solution(7082,7086,"Pippa"),
			new Solution(7329,7339,"Grandpa Pig"),
			new Solution(7345,7353,"Peppa Pig"),
			new Solution(7374,7377,"Penn"),
			new Solution(7383,7387,"Pippa"),
			new Solution(7402,7406,"Flick"),
			new Solution(7411,7419,"Cordelia."),
			new Solution(7558,7567,"Jack Wills"),
			new Solution(22218,22221,"Eton"),
			new Solution(22227,22232,"Rodean"),
			new Solution(22450,22456,"Britain"),
	};
	
	public void testOffsets(String filename, Solution[] gold)throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		StringBuilder sb = new StringBuilder();
		for(String line; (line = reader.readLine()) != null;) 
			sb.append(line).append("\n");
		reader.close();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GenerateNERTags generator = new TagSoupGenerator("classifiers/english.all.3class.distsim.crf.ser.gz");
		generator.reader(new StringReader(sb.toString()), new PrintStream(baos));
//		System.out.println(baos.toString());

		List<Solution> stanford = new ArrayList<Solution>();
		for (String s : baos.toString().split("\n")) {
			if (s.startsWith("NER")) {
				String[] parts = s.split("\t");
				stanford.add(new Solution(Integer.parseInt(parts[3]), Integer.parseInt(parts[4]),parts[5]));
			}
		}
		Collections.sort(stanford, new Comparator<Solution>() {
			@Override
			public int compare(Solution arg0, Solution arg1) {
				return new Integer(arg0.start).compareTo(arg1.start);
			}});
		reader = new BufferedReader(new StringReader(baos.toString()));
		int k=0,el=-1,correct=0;;
		stanford: for (Solution stan : stanford) {
//			if (!line.startsWith("NER")) continue;
			el++;
			System.out.println("NER\t"+stan);
			
//			String[] parts = line.split("\t");
			String np = stan.str;
			int start = stan.start;
			
			if (k>gold.length-1) break;
			e54: while(true) {
				System.out.print(gold[k].start +" "+gold[k].str);
				if (gold[k].str.equals(np)) {
					if (gold[k].start == start) {
						System.out.println(" Yay!");
						k++; correct++;
						continue stanford;
					} else System.out.print(" "+(gold[k].start - start));
				}
				System.out.println(" Nope");
				if (gold[k].start < start) {
					k++;
					if (k>=gold.length) break stanford;
					continue e54;
				} else {
					continue stanford;
				}
			}
		}
		
		System.out.println();
		System.out.println("Precision: "+((double)correct/stanford.size()));
		System.out.println("Recall: "+((double)correct/gold.length));
	}
	
	@Test
	public void testOffsets_discussion() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		testOffsets("bolt-eng-DF-170-181122-8792777.txt",bolt);
	}
	
	@Test
	public void testOffsets_newswire() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		testOffsets("XIN_ENG_20100609.0398.txt",xin);
	}
	@Test
	public void testOffsets_web() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
			testOffsets("eng-NG-31-100892-8857698.txt",eng);
	}
	
	@Test
	public void testOffsets_discussion2()throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		testOffsets("bolt-eng-DF-200-192392-4565951.txt",bolt2);
	}
	
	
	
	
	
	@Test
	public void checkToXML_discussion() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		checkToXML("bolt-eng-DF-170-181122-8792777.txt");
	}
	@Test
	public void checkToXML_discussion2() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		checkToXML("bolt-eng-DF-200-192392-4565951.txt");
	}
	@Test
	public void checkToXML_newswire() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		checkToXML("XIN_ENG_20100609.0398.txt");
	}
	@Test
	public void checkToXML_web() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		checkToXML("eng-NG-31-100892-8857698.txt");
	}
	public void checkToXML(String filename) throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		StringBuilder sb = new StringBuilder();
		for(String line; (line = reader.readLine()) != null;) 
			sb.append(line).append("\n");
		reader.close();

		Builder documentBuilder;
		XMLReader tagsoup = XMLReaderFactory.createXMLReader("org.ccil.cowan.tagsoup.Parser");
		tagsoup.setFeature("http://xml.org/sax/features/namespaces", false);
		documentBuilder = new Builder(tagsoup,false,new OffsetMaintainingNodeFactory());
		
		Document built = documentBuilder.build(new StringReader(sb.toString()));

		Element root = built.getRootElement();
		
		Elements docs = root.getChildElements();
		Element doc = docs.get(0);
		
		TagSoupGenerator generator = new TagSoupGenerator();
		List<String> text = generator.getText(doc, null);
		reader = new BufferedReader(new StringReader(text.get(0)));
		String[] lines = sb.toString().split("\n");
		String line = null;
		int charsofarO=0,charsofarR=0;
		for (int i=1; i<lines.length; i++) {
			if ( (line = reader.readLine()) == null) break;
			System.out.println(String.format("% 4d", charsofarO)+"O "+lines[i].replaceAll(" ","*"));
			System.out.println(String.format("% 4d", charsofarR)+"R "+line.replaceAll(" ","*"));
			charsofarO+=lines[i].length();
			charsofarR+=line.length();
		}
//		BufferedWriter writer = new BufferedWriter(new FileWriter("bolt-eng-DF-170-181122-8792777.toXML.txt"));
//		writer.write(doc.toXML());
//		writer.close();
	}
	
	@Test
	public void toy() {
		Element e = new Element("hello");
		e.appendChild("Hello, World!");
		Document d = new Document(e);
		
		for (int i=0; i<e.getChildCount(); i++) {
			Node n = e.getChild(i);
			if (n instanceof Text)
			System.out.println(e.getChild(i).toXML());
		}
		
		System.out.println(d.toXML());
		
	}

	@Test
	public void test() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(DOC.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
 
		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
 
		ObjectFactory o = new ObjectFactory();
		DOC doc = o.createDOC();
		doc.setDOCID(" groups.google.com_IdeaStockExchange_ae19e70842cbbcce_ENG_20050208_222900.LDC2006E32 ");
		doc.setDOCTYPE(o.createDOCTYPE());
		doc.getDOCTYPE().setvalue(" USENET TEXT ");
		doc.getDOCTYPE().setSOURCE("usenet");
		doc.setDATETIME(" 2005-02-08T22:29:00 ");
		doc.setBODY(o.createBODY());
		HEADLINE h = o.createHEADLINE(); h.setvalue("http://www.ornery.org/essays/warwatch/20 05-01-30-1.html");
		TEXT t = o.createTEXT();
		POST p = o.createPOST();
		POSTER poster = o.createPOSTER(); poster.setvalue(" myclob ");
		POSTDATE postdate = o.createPOSTDATE(); postdate.setvalue(" 2005-02-08T22:29:00 ");
		p.addValue(poster).addValue(postdate).addValue(
				"\nIt's ironic that a religious group that absolutely rejects "+
				"religious \nfreedom or even religious tolerance -- Wahhabism and Islamism -- \nshelters its "+
				"subversive, anti-American activities under the protection \nof the First Amendment.\n");
		t.getporTURNorPOSTorUNTRANSLATEDTEXT().add(p);
		doc.getBODY().addItem(h).addItem(t);
		
		
		jaxbMarshaller.marshal(doc, System.out);
		
		/*
		 * <DOC>
	<DOCID> groups.google.com_IdeaStockExchange_ae19e70842cbbcce_ENG_20050208_222900.LDC2006E32 </DOCID>
	<DOCTYPE SOURCE="usenet"> USENET TEXT </DOCTYPE>
	<DATETIME> 2005-02-08T22:29:00 </DATETIME>
	<BODY> 
	<HEADLINE>http://www.ornery.org/essays/warwatch/20 05-01-30-1.html</HEADLINE>
	<TEXT>
	<POST>
	<POSTER> myclob </POSTER>
	<POSTDATE> 2005-02-08T22:29:00 </POSTDATE>

		 */
	}

	
	@Test
	public void boltTest() throws IOException, JAXBException, ParserConfigurationException, SAXException, ValidityException, ParsingException {
		BufferedReader reader = new BufferedReader(new FileReader("bolt-eng-DF-170-181103-15978491.txt"));
		StringBuilder sb = new StringBuilder("<docs>");
		for(String line; (line = reader.readLine()) != null;) sb.append(line);
		sb.append("</docs>");
		reader.close();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		GenerateNERTags generator = new TagSoupGenerator("classifiers/english.all.3class.distsim.crf.ser.gz");
		generator.reader(new StringReader(sb.toString()), new PrintStream(baos));
		System.out.println(baos.toString());
	
	}
}
