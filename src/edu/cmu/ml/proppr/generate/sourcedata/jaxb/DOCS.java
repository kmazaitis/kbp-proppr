package edu.cmu.ml.proppr.generate.sourcedata.jaxb;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
			"DOC"
})
@XmlRootElement(name = "DOCS")
public class DOCS {
	@XmlElement(required = true)
	protected List<DOC> DOC;
	public List<DOC> getDOC() {
		if (DOC == null) {
			DOC = new ArrayList<DOC>();
		}
		return this.DOC;
	}
}
