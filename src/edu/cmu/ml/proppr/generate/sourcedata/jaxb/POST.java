//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.26 at 03:41:28 PM EDT 
//


package edu.cmu.ml.proppr.generate.sourcedata.jaxb;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = {
//    "values"
//})
@XmlRootElement(name = "POST")
public class POST {

    @XmlAttribute(name = "DATETIME")
//    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    protected String datetime;
    
    @XmlMixed
	@XmlElementRefs({
		@XmlElementRef(name="POSTER", type=POSTER.class),
		@XmlElementRef(name="POSTDATE", type=POSTDATE.class),
		@XmlElementRef(name="SUBJECT", type=SUBJECT.class),
		@XmlElementRef(name="QUOTE", type=QUOTE.class),
		@XmlElementRef(name="UNTRANSLATEDTEXT", type=UNTRANSLATEDTEXT.class)
	})
	protected List<Object> values;

    /**
     * Gets the value of the datetime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDATETIME() {
        return datetime;
    }

    /**
     * Sets the value of the datetime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDATETIME(String value) {
        this.datetime = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public List<Object> getvalues() {
    	if (values==null) values = new ArrayList<Object>();
        return values;
    }

    public POST addValue(Object o) {
    	this.getvalues().add(o);
    	return this;
    }

}
