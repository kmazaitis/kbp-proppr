package edu.cmu.ml.proppr.generate.sourcedata;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.Attribute;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.ParsingException;
import nu.xom.Serializer;
import nu.xom.Text;
import nu.xom.ValidityException;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import edu.cmu.ml.proppr.generate.Generate;

public class TagSoupGenerator extends GenerateNERTags {
	private static final byte space = new String(" ").getBytes()[0];
	private static final ByteArrayOutputStream baos = new ByteArrayOutputStream();
	private static final Serializer serializer = new Serializer(baos) {
		@Override protected void writeNamespaceDeclarations(Element e) {}
	};
	private static final String TYPE_PERSON = "PERSON";

	protected Generate parser;
	public TagSoupGenerator() {
		parser = new Generate();
	}
	public TagSoupGenerator(String serializedClassifier) {
		super(serializedClassifier);
		parser = new Generate();
	}
	
	@Override
	public void reader(Reader reader, PrintStream out) throws ValidityException, ParsingException, IOException {
		Document built = parser.getDocumentBuilder().build(reader);

		Element root = built.getRootElement();
		
		Elements docs = root.getChildElements();
		for (int d=0; d<docs.size(); d++) {
			Element doc = docs.get(d);
			String docName = getDocId(doc);
			docNumber+=1;
			out.println("DOC\t"+ (docNumber) +"\t"+docName);
			for (String text : getText(doc,out)) {
				text(text,out);
			}
		}
	}
	

	
	private String toXML(Element e) throws IOException {
		Document d = new Document(e);
		baos.reset();
		serializer.write(d);
		serializer.flush();
		return baos.toString().substring(40).replaceAll("~QUO~","&quot;");
	}
	private void stripQuotedMaterial(Element elt) {
		Elements contents = elt.getChildElements();
		for (int c=0; c<contents.size();c++) {
			Element cont = contents.get(c);
			if (cont.getLocalName().equalsIgnoreCase("quote")) {
				
				
				//change contents to whitespace
				String xml = cont.toXML();
				String save = "";
				if (cont.getAttribute("previouspost") != null) {
					String[] parts = xml.split(">", 2);
					parts[0] = parts[0].replaceAll("&#x0A;", "\n").replaceAll("~BQUO~", "&gt;").replaceAll(" \n","\n");
					save = parts[1].replaceAll("&amp;","and  ").replaceAll("\"","~QUO~").replace("</QUOTE>", "");
					xml = new StringBuilder(parts[0]).append(">").toString();//.append(parts[1]).toString();
				}
				char[] origv = xml.toCharArray();
				char[] newv = new char[origv.length];
				StringBuffer buf = new StringBuffer(8);
				int j=0;
				for (int i=0; i<origv.length; i++) {
					if (origv[i]=='\n' || origv[i]=='\r') 
						newv[j]=origv[i];
					else 
						newv[j]=' ';

					if (origv[i] == ' ') buf.delete(0, buf.length());
					else buf.append(origv[i]);
					// drop extra space added to some <img src="..."*/> tags
					if ( (buf.length() != 2) 
							|| (buf.charAt(0)!= '/') 
							|| (buf.charAt(1)!='>')) 
						j++;
				}
				elt.replaceChild(cont, new Text(new String(Arrays.copyOfRange(newv, 0, j)) + save));
			}
		}
	}
	private List<Element> recursiveGet(Element parent, String localName) {
		List<Element> result = null;
		Elements contents = parent.getChildElements();
		//leaf exit
		if (contents.size() == 0) return result;
		for (int i=0; i<contents.size(); i++) {
			Element c = contents.get(i);
			if (c.getLocalName().equals(localName)) {
				if (result == null) result = new ArrayList<Element>();
				result.add(c);
			}
			else {
				List<Element> childResult = recursiveGet(c,localName);
				if (childResult != null) {
					if (result == null) result = new ArrayList<Element>();
					result.addAll(childResult);
				}
			}
		}
		return result;
	}
	public List<String> getText(Element doc, PrintStream out) throws IOException {
		
		if (doc.getLocalName().equals("DOC")) {
			if (doc.getAttributeCount() > 0) {
				// newswire: AFP, APW, CNA, LTW, NYT, XIN (everybody but WPB)
				if (!doc.getAttribute("id").getValue().startsWith("WPB")) {
					Text first = (Text) doc.getChild(0);
					first.setValue(" "+first.getValue());
				}
			} else {
				// web
				List<Element> contents = recursiveGet(doc,"TEXT");
				for (int i=0; i<contents.size(); i++) {
					Elements posts = contents.get(i).getChildElements();
					// replace <QUOTE>s with whitespace:
					for (int p=0; p<posts.size(); p++) stripQuotedMaterial(posts.get(p));
				}
			}
		} else {
			// discussion
			Elements posts = doc.getChildElements();
			int charsofar=3+doc.getLocalName().length();
			for (int a=0; a<doc.getAttributeCount(); a++) {
				Attribute at = doc.getAttribute(a);
				charsofar+= 4 + at.getLocalName().length() + at.getValue().length();
			}
			StringBuilder docstr = new StringBuilder();
			for (int p=0; p<posts.size();p++) {
				Element post = posts.get(p);
				if (post.getLocalName().equals("post")) {
					if (out != null) {
						// print authors
						String author = post.getAttributeValue("author"); 
						print(out,author,charsofar+14,charsofar+14+author.length()-1,TYPE_PERSON,-1);
					}
					// replace <quote>s with whitespace:
					stripQuotedMaterial(post);
				}
				// keep track of current offset for author-attribute extractions
				String poststr = toXML((Element) post.copy());
				charsofar += poststr.length()-1;
			}
		}

		ArrayList<String> text = new ArrayList<String>();
		text.add(toXML((Element) doc.copy()));
//		text.add(docstr)
		return text;
	}
	private String getDocId(Element doc) { 
		if (doc.getAttribute("id") != null) {
			return doc.getAttributeValue("id");
		}
		Elements children = doc.getChildElements();
		for (int i=0; i<children.size(); i++) {
			Element elt = children.get(i);
			if (elt.getLocalName().equals("DOCID"))
				return elt.getValue();
		}
		System.out.println("No DOCID for document "+this.docNumber);
		return null;
	}
}
