package edu.cmu.ml.proppr.generate.sourcedata;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.Reader;
import java.rmi.UnmarshalException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.helpers.DefaultValidationEventHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import edu.cmu.ml.proppr.generate.Generate;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.BODY;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOC;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOCS;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.P;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POST;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TEXT;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TURN;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;


public abstract class GenerateNERTags {
	protected AbstractSequenceClassifier<CoreLabel> classifier;
	protected int docNumber;
	/** used for unit tests only */
	protected GenerateNERTags() {}
	public GenerateNERTags(String serializedClassifier) {
		super();
		this.classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
		this.docNumber=0;
	}
	
	public abstract void reader(Reader reader, PrintStream out) throws ValidityException, ParsingException, IOException;
	
	
	protected Reader read(File file) throws IOException {
		if (file.getName().endsWith(".gz")) {
			InputStream fileStream = new FileInputStream(file);
			InputStream gzipStream = new GZIPInputStream(fileStream);
			Reader decoder = new InputStreamReader(new GzDocsWrapper(gzipStream), "UTF-8");
			LineNumberReader buffered = new LineNumberReader(decoder);
			return buffered;
		}
		return new LineNumberReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
	}

	
	public void file(File file) {
		file(file,System.out);
	}
	public void file(File file, PrintStream out) {
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				file(f);
			}
		} else {
			System.err.println("FILE\t"+file.getName());
			Reader reader = null;
			try {
				reader = read(file);
				reader(reader,out);
				
			} catch (UnmarshalException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ValidityException e) {
				e.printStackTrace();
			} catch (ParsingException e) {
				e.printStackTrace();
			} 
		}
	}


	public void text(String text) {
		text(text,System.out);
	}
	public void text(String text, PrintStream out){
		HashMap<String,TreeSet<Integer>> termVector = new HashMap<String,TreeSet<Integer>>();
		int i=0;
		StringBuilder multiWord=null;
		int multiWord_begin=0;
		int multiWord_end=0;
		String last_ner="";
		int sentenceid=0;
		for (List<CoreLabel> sentence : classifier.classify(text)) {
			sentenceid++;
			for (CoreLabel word : sentence) {
				if (word.get(CoreAnnotations.ShapeAnnotation.class).toLowerCase().startsWith("x")) {
					if (!word.word().startsWith("http://")) {
						String term = word.word().toLowerCase();
						if (!termVector.containsKey(term)) termVector.put(term, new TreeSet<Integer>());
						termVector.get(term).add(sentenceid);
					}
					String ner = word.get(CoreAnnotations.AnswerAnnotation.class);
					if (!"O".equals(ner)) {
						if (null==multiWord) {
							// O X -> new
							multiWord = new StringBuilder(word.word());
							multiWord_begin = word.beginPosition();
							multiWord_end = word.endPosition();
						} else if (last_ner.equals(ner)) {
							// X X -> append
							multiWord.append(" ").append(word.word());
							multiWord_end = word.endPosition();
						} else  {
							// X Y -> print; new
							print(out,multiWord.toString(),multiWord_begin,multiWord_end,last_ner,sentenceid);

							multiWord = new StringBuilder(word.word());
							multiWord_begin = word.beginPosition();
							multiWord_end = word.endPosition();
						}

					} else if (multiWord != null) {
						// X O -> print; end
						print(out,multiWord.toString(),multiWord_begin,multiWord_end,last_ner,sentenceid);
						multiWord=null;
					}
					last_ner=ner;
				}
			} // end of sentence
			if (multiWord != null) {
				// X. -> print; end
				print(out,multiWord.toString(),multiWord_begin,multiWord_end,last_ner,sentenceid);
				multiWord = null;
			}
		}
		for (String word : termVector.keySet()) {
			StringBuilder sentences = new StringBuilder();
			for (Integer s : termVector.get(word)) sentences.append(" ").append(s);
			out.println("TOK\t"+this.docNumber+"\t"+sentences.substring(1)+"\t"+word);
		}
	}

	public void print(PrintStream out, String name, int begin, int end, String ner, int sentence) {
		out.println("NER\t"+this.docNumber+"\t"+sentence+"\t"+begin+"\t"+end+"\t"+name+"\t"+ner);
	}


	/**
	 * @param args
	 * @throws JAXBException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public static void main(String[] args) throws JAXBException, ParserConfigurationException, SAXException {
		String serializedClassifier = "classifiers/english.all.3class.distsim.crf.ser.gz";

		if (args.length > 0) {
			serializedClassifier = args[0];
		}

		GenerateNERTags generator = new TagSoupGenerator(serializedClassifier);

		if (args.length > 1) {
			for (int i=1; i<args.length; i++) {
				File toparse = new File(args[i]);
				generator.file(toparse);
			}
		} else {
			System.out.println("Usage:\n\t[classifiermodel.ser.gz [file ...]]\n\nRunning demo:");
			
			String s1 = "Good afternoon Rajat Raina, how are you today? ";
			String s2 = "I go to school at Stanford University, which is located in California.";
			generator.text(s1+" "+s2);
		}
	}
}
