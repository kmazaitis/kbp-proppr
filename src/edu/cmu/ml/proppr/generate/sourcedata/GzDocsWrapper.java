package edu.cmu.ml.proppr.generate.sourcedata;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.SequenceInputStream;
import java.io.StringBufferInputStream;
import java.util.Vector;

//import org.w3c.tidy.Tidy;

public class GzDocsWrapper extends InputStream {

	private SequenceInputStream stream;

	public GzDocsWrapper(InputStream in) {
		Vector<InputStream> streams = new Vector<InputStream>();
		streams.add(new StringBufferInputStream("<DOCS>"));
		streams.add(in);
		streams.add(new StringBufferInputStream("</DOCS>"));
		this.stream = new SequenceInputStream(streams.elements());
	}

	@Override
	public int read() throws IOException {
		int current=-1;
		while((current = this.stream.read()) > 0)
			if ((current == 0x9) ||
					(current == 0xA) ||
					(current == 0xD) ||
					((current >= 0x20) && (current <= 0xD7FF)) ||
					((current >= 0xE000) && (current <= 0xFFFD)) ||
					((current >= 0x10000) && (current <= 0x10FFFF)))
				return current;
		return current;
	}
}
