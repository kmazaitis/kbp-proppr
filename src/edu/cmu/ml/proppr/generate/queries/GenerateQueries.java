package edu.cmu.ml.proppr.generate.queries;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import edu.cmu.ml.proppr.generate.Generate;


public class GenerateQueries extends Generate {
	public GenerateQueries() throws JAXBException, ParserConfigurationException, SAXException {
		super();
	}
	public void file(File file) {
		System.err.println("FILE\t"+file.getName());
		Reader reader = null;
		int q=-1;
		try {
			reader = read(file);
			Document built = documentBuilder.build(reader);
			Element root = built.getRootElement();
			Elements queries = root.getChildElements();
			for (q=0; q<queries.size(); q++) {
				Element query = queries.get(q);
				String queryId = query.getAttributeValue("id");
				HashMap<String,String> data = new HashMap<String,String>();
				Elements children = query.getChildElements();
				for (int i=0; i<children.size(); i++) {
					Element elt = children.get(i);
					data.put(elt.getLocalName(), elt.getValue());
				}
				StringBuilder sb = new StringBuilder(queryId);
				sb.append("\t").append(data.get("docid"));
				sb.append("\t").append(data.get("beg"));
				sb.append("\t").append(data.get("end"));
				sb.append("\t").append(data.get("name"));
				System.out.println(sb.toString());
			}
		} catch (Exception e) {
			handle(e,q);
		}
		
	}
	
	private void handle(Exception e, int q) {
		e.printStackTrace();
		System.err.println("...at query "+q);
	}
	
	/**
	 * @param args
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws JAXBException 
	 */
	public static void main(String[] args) throws JAXBException, ParserConfigurationException, SAXException {
		if (args.length == 0) {
			System.err.println("Usage:\n\tqueryFile.xml > queryFile.tsv");
		}
		GenerateQueries generator = new GenerateQueries();
		for (String f : args) {
			File queryFile = new File(f);
			generator.file(queryFile);
		}
	}

}
