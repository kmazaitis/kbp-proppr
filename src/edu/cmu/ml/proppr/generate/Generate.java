package edu.cmu.ml.proppr.generate;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.zip.GZIPInputStream;

import nu.xom.Builder;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import edu.cmu.ml.proppr.generate.sourcedata.GzDocsWrapper;

public class Generate {
	protected Builder documentBuilder;
	public Generate() {
		try {      
			XMLReader tagsoup = XMLReaderFactory.createXMLReader("org.ccil.cowan.tagsoup.Parser");
			tagsoup.setFeature("http://xml.org/sax/features/namespaces", false);
			tagsoup.setFeature("http://www.ccil.org/~cowan/tagsoup/features/ignorable-whitespace",true);
			documentBuilder = new Builder(tagsoup,false,new OffsetMaintainingNodeFactory());
			// ...
		}
		catch (SAXException ex) {
			System.out.println("Could not load TagSoup.");
			System.out.println(ex.getMessage());
		}
	}
	protected Reader read(File file) throws IOException {
		if (file.getName().endsWith(".gz")) {
			InputStream fileStream = new FileInputStream(file);
			InputStream gzipStream = new GZIPInputStream(fileStream);
			Reader decoder = new InputStreamReader(new GzDocsWrapper(gzipStream), "UTF-8");
			LineNumberReader buffered = new LineNumberReader(decoder);
			return buffered;
		}
		return new LineNumberReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
	}
	public Builder getDocumentBuilder() {
		return this.documentBuilder;
	}
}
