package edu.cmu.ml.proppr.generate;

import nu.xom.Attribute;
import nu.xom.NodeFactory;
import nu.xom.Nodes;

public class OffsetMaintainingNodeFactory extends NodeFactory {

	public OffsetMaintainingNodeFactory() {
		super();
	}

	@Override
	public Nodes makeAttribute(String name,
            String URI,
            String value,
            Attribute.Type type) {
		Nodes result = super.makeAttribute(name,URI,value,type);
		
		if (name.equals("previouspost")) {
			StringBuilder sb = new StringBuilder("\n")
				.append(value.replaceAll(" >","\n~BQUO~").trim())
				.append("\n");
			result = super.makeAttribute(name,URI,sb.toString(),type);
		}
		
		return result;
	}
	
}
