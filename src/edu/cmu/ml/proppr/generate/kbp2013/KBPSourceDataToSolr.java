package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOC;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.P;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POST;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TEXT;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TURN;
/**
 * Processes KBP source documents XML data into "doc" entities grokkable by Solr.
 * 
 * @author "Kathryn Mazaitis <krivard@cs.cmu.edu>"
 *
 */
public class KBPSourceDataToSolr extends KBPToSolr {
	
	public KBPSourceDataToSolr(String outfile) throws JAXBException, IOException {
		super(outfile, DOC.class);
	}

	@Override
	protected List<? extends Object> toInner(Object doc_o) {
		ArrayList<Object> documents = new ArrayList<Object>();
		documents.add(doc_o);
		return documents;
	}

	@Override
	protected String toDocXml(Object doc_o) {
		DOC root = ((DOC) doc_o);
		StringBuilder sb = new StringBuilder("<doc>\n");
		sb.append("<field name=\"id\">").append(root.getDOCID()).append("</field>\n");
		sb.append("<field name=\"name\">").append(root.getDOCID()).append("</field>\n");
		
		sb.append("<field name=\"body\">");
		for(Object o : root.getBODY().getSLUGOrKEYWORDOrHEADEROrHEADLINEOrTEXTOrFOOTEROrTRAILER()) {
			if (o instanceof TEXT) {
				for (Object o2 : ((TEXT) o).getporTURNorPOSTorUNTRANSLATEDTEXT()) {
					if (o2 instanceof POST) {
						for (Object o3 : ((POST) o2).getvalues()) {
							if (o3 instanceof String) {
								if ( ((String) o3).trim().length() > 0) {
									sb.append(sanitize((String) o3)).append("\n");
								}
							}
						}
					} else if (o2 instanceof TURN) {
						sb.append(sanitize(((TURN) o2).getvalue())).append("\n");
					} else if (o2 instanceof P) {
						sb.append(sanitize(((P) o2).getvalue())).append("\n");
					}
				}
			}
		}
		sb.append("</field>\n");
		sb.append("</doc>\n");
		return sb.toString();

	}

	/**
	 * @param args
	 * @throws IOException 
	 * @throws JAXBException 
	 */
	public static void main(String[] args) throws JAXBException, IOException {
		if (args.length<2) {
			System.out.println("Usage:\n\tsolrOutput.xml {file|dir ... }");
			System.exit(0);
		}
		String output = args[0];
		KBPSourceDataToSolr k = new KBPSourceDataToSolr(output);
		k.start();
		int n=0;
		for (int i=1; i<args.length; i++) {
			n+=k.process(new File(args[i]));
		}
		k.finish();
		System.out.println("Processed "+n+" documents.");
	}

}
