package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import edu.cmu.ml.proppr.generate.kb.jaxb.Entity;
import edu.cmu.ml.proppr.generate.kb.jaxb.KnowledgeBase;

/**
 * Processes KBP KB XML data into "doc" entities grokkable by Solr.
 * 
 * @author "Kathryn Mazaitis <krivard@cs.cmu.edu>"
 *
 */
public class KBPKBToSolr extends KBPToSolr {
	public KBPKBToSolr(String outputfile) throws JAXBException, IOException {
		super(outputfile,KnowledgeBase.class);
	}

	@Override
	protected String toDocXml(Object o) {
		Entity e = (Entity) o;
		StringBuilder sb = new StringBuilder("<doc>\n");
		sb.append("<field name=\"id\">").append(e.getId()).append("</field>\n");
		sb.append("<field name=\"name\">").append(
				sanitize(e.getName())).append("</field>\n");
		sb.append("<field name=\"wiki_title\">").append(
				sanitize(e.getWikiTitle())).append("</field>\n");
		sb.append("<field name=\"wiki_text\">").append(
				sanitize(e.getWikiText())).append("</field>\n");
		sb.append("</doc>\n");
		return sb.toString();
		
	}
	@Override
	protected List<? extends Object> toInner(Object root) {
		return ((KnowledgeBase) root).getEntity();
	}
	
	/**
	 * @param args
	 * @throws JAXBException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws JAXBException, IOException {
		if (args.length<2) {
			System.out.println("Usage:\n\tsolrOutput.xml {file|dir ... }");
			System.exit(0);
		}
		String output = args[0];
		KBPKBToSolr k = new KBPKBToSolr(output);
		k.start();
		int n=0;
		for (int i=1; i<args.length; i++) {
			n+=k.process(new File(args[i]));
		}
		k.finish();
		System.out.println("Processed "+n+" documents.");
	}

}
