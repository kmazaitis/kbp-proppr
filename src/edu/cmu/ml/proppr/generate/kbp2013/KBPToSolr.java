package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import edu.cmu.ml.proppr.generate.kb.jaxb.Entity;
import edu.cmu.ml.proppr.generate.kb.jaxb.KnowledgeBase;

public abstract class KBPToSolr {

	protected Unmarshaller unmarshaller;
	protected BufferedWriter writer;

	public KBPToSolr(String outputfile, Class clazz) throws JAXBException, IOException {
		JAXBContext jaxb = JAXBContext.newInstance(clazz);
		unmarshaller = jaxb.createUnmarshaller();
		writer = new BufferedWriter(new FileWriter(outputfile));
	}

	public void start() throws IOException {
		writer.write("<add>\n");
	}

	public void finish() throws IOException {
		writer.write("</add>\n");
		writer.close();
	}

	protected String sanitize(String s) {
		return s
				.replaceAll("&","&amp;")
				.replaceAll("<","&lt;")
				.replaceAll(">","&gt;");
	}

	private void extract(File file) throws JAXBException, IOException {
		Object root = unmarshaller.unmarshal(file);
		List<? extends Object> inner = toInner(root);
		for (Object e : inner) {
			writer.write(toDocXml(e));
		}
	}

	protected abstract List<? extends Object> toInner(Object root);
	protected abstract String toDocXml(Object o);

	protected int process(File file) throws IOException {
		if (file.isDirectory()) {
			int n=0;
			for (File f: file.listFiles()) n+= process(f);
			return n;
		} else {
			try {
				extract(file);
				return 1;
			} catch (JAXBException e) {
				System.err.println("Couldn't process file "+file.getPath());
				e.printStackTrace();
			}
			return 0;
		}
	}

}