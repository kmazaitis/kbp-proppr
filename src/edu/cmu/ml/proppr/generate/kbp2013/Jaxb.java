package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOC;

public class Jaxb {
	public static void main(String[] args) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(DOC.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        File xml = new File(args[0]);
        DOC root = (DOC) unmarshaller.unmarshal(xml);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, System.out);
	}
}
