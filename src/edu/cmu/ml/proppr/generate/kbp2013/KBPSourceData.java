package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOC;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.P;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POST;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TEXT;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TURN;

public class KBPSourceData extends KBPExtractor {
	private static final Logger log = Logger.getLogger(KBPSourceData.class);
	public KBPSourceData() throws JAXBException {
		super(DOC.class);
	}
	public KBPSourceData(File file, File sentence, File np, File facts)
			throws JAXBException, IOException {
		super(file, sentence, np, facts, DOC.class);
	}
	/**
	 * @param args
	 * @throws IOException 
	 * @throws JAXBException 
	 */
	public static void main(String[] args) throws JAXBException {
		if (args.length<5) {
			System.out.println("Usage:\n\tfileOutput sentenceOutput npOutput factsOutput {file|dir ...}");
			System.exit(0);
		}
		try {
			KBPExtractor k = new KBPSourceData(new File(args[0]),new File(args[1]),new File(args[2]),new File(args[3]));
			int n=0;
			log.info("Initialization complete. Starting to parse files...");
			for (int i=4; i<args.length; i++) {
				n += k.process(new File(args[i]));
			}
			System.out.println("Processed "+n+" documents.");
			k.done();
		} catch (IOException e) {
			System.err.println("Filesystem trouble (likely output file)");
			e.printStackTrace();
		}
	}
	
	@Override
	protected List<String> getBlocks(Object doc_o) {
		ArrayList<String> documents = new ArrayList<String>();
		DOC root = ((DOC) doc_o);
		for(Object o : root.getBODY().getSLUGOrKEYWORDOrHEADEROrHEADLINEOrTEXTOrFOOTEROrTRAILER()) {
			if (o instanceof TEXT) {
				for (Object o2 : ((TEXT) o).getporTURNorPOSTorUNTRANSLATEDTEXT()) {
					if (o2 instanceof POST) {
						for (Object o3 : ((POST) o2).getvalues()) {
							if (o3 instanceof String) {
								if ( ((String) o3).trim().length() > 0) {
									documents.add((String) o3);
								}
							}
						}
					} else if (o2 instanceof TURN) {
						documents.add(((TURN) o2).getvalue());
					} else if (o2 instanceof P) {
						documents.add(((P) o2).getvalue());
					}
				}
			}
		}
		return documents;
	}
	@Override
	protected String getDocid(Object doc) {
		return String.valueOf(fileId);//((DOC) doc).getDOCID();
	}
	@Override
	protected List<Object> getDocuments(Object root) {
		return Collections.singletonList(root);
	}
}
