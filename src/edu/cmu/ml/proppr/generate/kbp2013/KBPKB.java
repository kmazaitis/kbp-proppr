package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import edu.cmu.ml.proppr.generate.kb.jaxb.Fact;
import edu.cmu.ml.proppr.generate.kb.jaxb.KnowledgeBase;
import edu.cmu.ml.proppr.generate.kb.jaxb.Link;

public class KBPKB extends KBPExtractor {
	private static final Logger log = Logger.getLogger(KBPKB.class);
	private boolean factsonly;

	public KBPKB(boolean factsonly, File file, File sentence, File np, File facts)
			throws JAXBException, IOException {
		super(file, sentence, np, facts, KnowledgeBase.class);
		this.factsonly=factsonly;
	}

	@Override
	protected List<? extends Object> getDocuments(Object root) {
		if (this.factsonly) return Collections.emptyList();
		return ((KnowledgeBase) root).getEntity();
	}

	@Override
	protected List<String> getBlocks(Object entity_o) {
		edu.cmu.ml.proppr.generate.kb.jaxb.Entity entity = ((edu.cmu.ml.proppr.generate.kb.jaxb.Entity) entity_o);
		ArrayList<String> blocks = new ArrayList<String>();
		blocks.add(entity.getWikiText().replaceAll("\n\n", ".\n\n"));
		// todo: facts?
		return blocks;
	}

	@Override
	protected String getDocid(Object entity) {
		return ((edu.cmu.ml.proppr.generate.kb.jaxb.Entity) entity).getId();
	}
	
	private String sanitize(String str) {
		return str.toLowerCase()
				.replaceAll("[()/\\\\]","_")
				.replaceAll("-","_")
				.replaceAll("[^A-Za-z0-9_$%&]","");
	}
	
	@Override
	protected void writeFacts(Object root, BufferedWriter writer) throws IOException {
		KnowledgeBase kb = (KnowledgeBase) root;
		for (edu.cmu.ml.proppr.generate.kb.jaxb.Entity entity : kb.getEntity()) {
			StringBuilder sb = new StringBuilder();
			String eid_arg = sanitize(entity.getId())+",";
			sb.append("#\t").append(entity.getWikiTitle()).append("\t").append(eid_arg.substring(0,eid_arg.length()-1)).append("\n");
			sb.append("entityName(").append(eid_arg).append(sanitize(entity.getWikiTitle().replaceAll("_*\\(.*\\)", ""))).append(")\n");
			sb.append("entityType(").append(eid_arg).append(sanitize(entity.getType())).append(")\n");
			for (Fact fact : entity.getFacts().getFact()) {
				for (Object v : fact.getValues()) {
					if (v instanceof Link) {
						Link link = (Link) v;
						if (link.getEntityId() != null) {
							sb.append("hyperlink(").append(eid_arg).append(sanitize(link.getEntityId())).append(")\n");
						}
					}
				}
			}
			writer.write(sb.toString());
		}
	}

	/**
	 * @param args
	 * @throws JAXBException 
	 */
	public static void main(String[] args) throws JAXBException {
		if (args.length<5) {
			System.out.println("Usage:\n\t[factsonly] fileOutput sentenceOutput npOutput factsOutput {file|dir ...}");
			System.exit(0);
		}
		try {
			int start = 0;
			boolean factsonly = false;
			if (args[0].equals("factsonly")) {
				start=1;
				factsonly=true;
			}
			KBPExtractor k = new KBPKB(factsonly, new File(args[start]),new File(args[start+1]),new File(args[start+2]),new File(args[start+3]));
			int n=0;
			log.info("Initialization complete. Starting to parse files...");
			for (int i=start+4; i<args.length; i++) {
				n += k.process(new File(args[i]));
			}
			System.out.println("Processed "+n+" documents.");
			k.done();
		} catch (IOException e) {
			System.err.println("Filesystem trouble (likely output file)");
			e.printStackTrace();
		}
	}

}
