package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import edu.cmu.ml.proppr.generate.sourcedata.jaxb.DOC;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.POST;
import edu.cmu.ml.proppr.generate.sourcedata.jaxb.TEXT;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;

public abstract class KBPExtractor {
	private static final Logger log = Logger.getLogger(KBPExtractor.class);
	private static final int MAX_NP_TOKENS = 6;
	private StanfordCoreNLP pipeline;
	private Unmarshaller unmarshaller;

	private BufferedWriter sentenceData, npData, fileData, factsData;
	protected int fileId=0;

	public KBPExtractor(Class clazz) throws JAXBException {
		JAXBContext jaxb = JAXBContext.newInstance(clazz);
		unmarshaller = jaxb.createUnmarshaller();

		Properties props = new Properties();
		props.put("annotators", "tokenize, cleanxml, ssplit, pos, lemma, ner");
		//		props.put("ssplit.boundaryTokenRegex", "\\.|[!?]+|\n\n");
		pipeline = new StanfordCoreNLP(props);
	}
	public KBPExtractor(File file, File sentence, File np, File facts, Class clazz) throws JAXBException, IOException {
		this(clazz);
		fileData = new BufferedWriter(new FileWriter(file));
		sentenceData = new BufferedWriter(new FileWriter(sentence));
		npData = new BufferedWriter(new FileWriter(np));
		factsData = new BufferedWriter(new FileWriter(facts));
	}

	public static class Entity {
		public String name;
		public String ne;
	}

	private List<Entity> collectNPs(Tree tree, Map<String, String> entity_ne) {
		ArrayList<Entity> ret = new ArrayList<Entity>();
		collectNPs_t(tree,entity_ne,ret);
		return ret;
	}
	private boolean collectNPs_t(Tree tree, Map<String, String> entity_ne, List<Entity> nps) {
		boolean np=false;int k=0;
		for (Tree t : tree.children()) {
			k++;
			if (t.children().length > 0) {
				boolean tnp = collectNPs_t(t,entity_ne,nps);
				np = np || tnp;
			}
		}
		if (k==0) return false;

		if (!np && tree.value().equals("NP")) {
			List<Tree> leaves = tree.getLeaves();
			if (leaves.size() > MAX_NP_TOKENS) {
				return false;
			}
			Entity e = new Entity();
			StringBuilder sb = new StringBuilder();
			for (Tree leaf : tree.getLeaves()) {
				if (entity_ne.containsKey(leaf.nodeString())) e.ne = entity_ne.get(leaf.nodeString());
				sb.append(" ").append(leaf.nodeString()); 
			}
			e.name = sb.substring(1);
			nps.add(e);
			return true;
		}
		return np;
	}

	public void done() throws IOException {
		if (fileData != null) fileData.close();
		if (sentenceData != null) sentenceData.close();
		if (npData != null) npData.close();
		if (factsData != null) factsData.close();
	}

	public int demo(File file) throws JAXBException, IOException {
		fileId++;
		if (fileData!= null) {
			fileData.write(fileId+"\t"+file.getName()+"\n");
		}
		Object root = unmarshaller.unmarshal(file);
		List<? extends Object> docs = getDocuments(root);
		int nblocks=0;
		for (Object doc : docs) {
			String docid = getDocid(doc);
			List<String> blocks = getBlocks(doc);
			nblocks+=blocks.size();
			int sentenceId = 0;
			for(int i=0; i<blocks.size(); i++) {
				String block = blocks.get(i);
				try {
					sentenceId = annotate(sentenceId, docid, block);
				} catch (Exception e) {
					log.error("Trouble with block "+i+" in "+file.getName()+":"+docid+" (skipping)",e);
				}
			}
		}
		writeFacts(root, factsData);
		if (fileData != null) {
			fileData.flush();
			sentenceData.flush();
			npData.flush();
			factsData.flush();
		}
		return nblocks;
	}

	protected abstract List<? extends Object> getDocuments(Object root);
	protected abstract List<String> getBlocks(Object doc);
	protected abstract String getDocid(Object doc);
	protected void writeFacts(Object root, BufferedWriter writer) throws IOException {}

	protected int annotate(int sentenceId, String docid, String text) throws IOException {

		// create an empty Annotation just with the given text
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		pipeline.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		Map<String,String> entity_ne; StringBuilder sb,nesb; String lasttag="O"; Entity ent;
		for(CoreMap sentence: sentences) {
			sentenceId++; // 1-indexed
			String file_sentence = fileId+"\t"+docid+"\t"+sentenceId;
			//clear ne map
//			entity_ne = new HashMap<String,String>();
			sb = new StringBuilder();
			nesb=new StringBuilder();
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// this is the POS tag of the token
				//	        String pos = token.get(PartOfSpeechAnnotation.class);
				//	        // this is the NER label of the token
				String ne = token.get(NamedEntityTagAnnotation.class);  
				sb.append(" ").append(word);
				if (ne != null && !ne.equals("O")) {
					sb.append("("+ne+")");
//					if (entity_ne.containsKey(word) && !entity_ne.get(word).equals(ne)) {
//						log.warn(word+" currently tagged ("+entity_ne.get(word)+") in sentence "+sentenceId+". Changing to ("+ne+")");
//					}
//					entity_ne.put(word, ne);

					if (!lasttag.equals(ne)) {
						if (!lasttag.equals("O")) { 
							if (nesb.length() > 0) {
								log.debug("NE"+nesb.toString()+" "+lasttag);
								if (npData != null) {
									npData.write(file_sentence+"\t"+nesb.substring(1)+"\t"+lasttag+"\n");
								}
							}
						}
						nesb = new StringBuilder();
					}
					nesb.append(" ").append(word);
				} else if (!lasttag.equals("O")) {
					if (nesb.length() > 0) {
						log.debug("NE"+nesb.toString()+" "+lasttag);
						if (npData != null) {
							npData.write(file_sentence+"\t"+nesb.substring(1)+"\t"+lasttag+"\n");
						}
					}
					nesb = new StringBuilder();
				}
				if (npData != null) {
					npData.write(file_sentence+"\t"+word+"\t-\n");
				}
				lasttag=ne;
			}
			String sentencestr = sb.substring(1);
			if (!sentencestr.matches(".*[a-zA-Z0-1].*")) continue;
			log.debug(" " + sentencestr);
			if (sentenceData != null) {
				sentenceData.write(file_sentence+
						"\t"+sentencestr+
						"\n");
			}

			// this is the parse tree of the current sentence
			//			Tree tree = sentence.get(TreeAnnotation.class);
			//			int npId=0;
			//			for (Entity s : collectNPs(tree, entity_ne)) {
			//				npId++; // 1-indexed
			//				sb=new StringBuilder("NP ").append(s.name);
			//				if (s.ne != null) sb.append(" ("+s.ne+")");
			//				log.debug(sb.toString());
			//				if (npData != null) {
			//					npData.write(file_sentence+
			//							"\t"+npId+
			//							"\t"+s.name+
			//							"\t"+ (s.ne != null ? s.ne : "-")+
			//							"\n");
			//				}
			//			}
			// this is the Stanford dependency graph of the current sentence
			//	      SemanticGraph dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		}

		// This is the coreference link graph
		// Each chain stores a set of mentions that link to each other,
		// along with a method for getting the most representative mention
		// Both sentence and token offsets start at 1!
		//	    Map<Integer, CorefChain> graph = 
		//	      document.get(CorefChainAnnotation.class);
		return sentenceId;
	}


	protected int process(File file) throws IOException {
		if (file.isDirectory()) {
			int n=0;
			for (File f : file.listFiles()) n+=process(f);
			return n;
		} else {
			try {
				int blocks = demo(file);
				System.out.println(blocks+"\t"+file.getName());
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				log.error("Problem with file "+file.getAbsolutePath(),e);
			}
			return 1;
		}
	}

}
