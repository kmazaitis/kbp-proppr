package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.util.BytesRef;

/**
 * Goes through every document in a lucene index and makes a
 * 
 *   predicateName<TAB>docId<TAB>term
 *   
 * for each term in the document. (This is ProPPR .cfacts format)
 * @author "Kathryn Mazaitis <krivard@cs.cmu.edu>"
 *
 */
public class WordsFromLucene {

	protected static final String DEFAULT_LUCENE_FIELD = "wiki_text";
	protected static final String CFACTS_DELIM = "\t";
	protected static final String ID_FIELD = "id";

	protected Directory dir;
	protected IndexReader reader=null;
	protected BufferedWriter writer;
	protected String field;
	protected String predicate;
	public WordsFromLucene(String indexDir, String predicate, String output, String field) throws IOException {
		dir = new NIOFSDirectory(new File(indexDir));
		if (DirectoryReader.indexExists(dir)) {
			reader = DirectoryReader.open(dir);
		} else {
			System.out.println("No index at "+indexDir);
			System.exit(0);
		}
		if (reader.hasDeletions()){
			System.out.println("Index has deleted docs! Can't handle this yet.\n");
			System.exit(0);
		}
		
		writer = new BufferedWriter(new FileWriter(output));
		this.predicate = predicate;
		this.field = field;
	}
	
	public void writeWords() throws IOException {
		TermsEnum te = null;
		for (int i=0; i<reader.maxDoc(); i++) {
			Document d = reader.document(i);
			Terms terms = reader.getTermVector(i, field);
			if (terms == null) {
				System.out.println("Index didn't store term vectors for "+DEFAULT_LUCENE_FIELD+"! You must re-index. :(");
				System.exit(0);
			}
			String id = id(d);
			te = terms.iterator(te);
		    BytesRef term; StringBuilder sb;
		    while((term = te.next()) != null) {
		    	sb = new StringBuilder(predicate).append(CFACTS_DELIM).append(id).append(CFACTS_DELIM).append(term.utf8ToString()).append("\n");
		    	writer.write(sb.toString());
		    }
		}
	}
	protected String id(Document d) {
		return d.get(ID_FIELD);
	}
	public void close() throws IOException {

		writer.close();
		reader.close();
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage:\n\tpath/to/index/ predicateName outputFile.cfacts [lucenetextfield]\n"+
					"\t(default lucenetextfield is 'wiki_text')");
			System.exit(0);		
		}
		String indexDir = args[0],
				predicate = args[1],
				output = args[2],
				field = DEFAULT_LUCENE_FIELD;
		if (args.length >=4) field = args[3];

		WordsFromLucene w = new WordsFromLucene(indexDir, predicate, output, field);
		w.writeWords();
		w.close();
	}

}
