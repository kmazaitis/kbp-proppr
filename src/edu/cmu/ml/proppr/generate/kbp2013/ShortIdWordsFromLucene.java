package edu.cmu.ml.proppr.generate.kbp2013;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.apache.lucene.document.Document;
/**
 * Goes through every document in a lucene index and makes a
 * 
 *   predicateName<TAB>docnum<TAB>term
 *   
 * for each term in the document (this is ProPPR .cfacts format)
 * where docnum is a shorter id for the document, specified in a lookup table (in .cfacts format).
 * @author "Kathryn Mazaitis <krivard@cs.cmu.edu>"
 *
 */
public class ShortIdWordsFromLucene extends WordsFromLucene {

	HashMap<String,String> docids = new HashMap<String,String>();
	public ShortIdWordsFromLucene(String indexDir, String ids, String predicate,
			String output, String field) throws IOException {
		super(indexDir, predicate, output, field);
		
		BufferedReader reader = new BufferedReader(new FileReader(ids));
		for(String line; (line=reader.readLine())!=null; ) {
			String[] parts = line.split(WordsFromLucene.CFACTS_DELIM);
			docids.put(parts[2], parts[1]);
		}
		reader.close();
	}
	
	@Override
	protected String id(Document d) {
		String id = d.get(ID_FIELD).trim();
		if (!docids.containsKey(id)) throw new IllegalArgumentException("no known id '"+id+"'");
		return docids.get(id);
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage:\n\tpath/to/index/ documentId_did_dname.cfacts predicateName outputFile.cfacts [lucenetextfield]\n"+
					"\t(default lucenetextfield is 'wiki_text')");
			System.exit(0);		
		}
		String indexDir = args[0],
				ids = args[1],
				predicate = args[2],
				output = args[3],
				field = DEFAULT_LUCENE_FIELD;
		if (args.length >=5) field = args[4];

		ShortIdWordsFromLucene w = new ShortIdWordsFromLucene(indexDir, ids, predicate, output, field);
		w.writeWords();
		w.close();
	}
}
